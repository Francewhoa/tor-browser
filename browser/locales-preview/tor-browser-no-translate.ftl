# tor-browser strings that should *not* be sent to translators on Weblate.

## Bridge pass invite dialog.
## Temporary until tor-browser#42385

lox-invite-dialog-title =
    .title = Bridge pass invites
lox-invite-dialog-description = You can ask the bridge bot to create a new invite, which you can share with a trusted contact to give them their own bridge pass. Each invite can only be redeemed once, but you will unlock access to more invites over time.
lox-invite-dialog-request-button = Request new invite
lox-invite-dialog-connecting = Connecting to bridge pass server…
lox-invite-dialog-no-server-error = Unable to connect to bridge pass server.
lox-invite-dialog-generic-invite-error = Failed to create a new invite.
lox-invite-dialog-invites-label = Created invites:
lox-invite-dialog-menu-item-copy-invite =
    .label = Copy invite
    .accesskey = C
